require_relative '../lib/card'
require_relative '../lib/suits'
require_relative '../lib/ranks'

describe Card do
	it 'initializes a card' do
        :given
        card = Card.new(Suits::DIAMONDS, Ranks::ACE)

        :expect
        card.suit.should == Suits::DIAMONDS
        card.rank.should == Ranks::ACE
    end
end