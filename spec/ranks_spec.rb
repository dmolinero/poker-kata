require_relative '../lib/ranks'

describe Ranks do
	
	class HandRanks 

		include Ranks

		attr_reader :ranks

		def initialize(*ranks)
			@ranks = ranks
		end
	end

	it 'returns if ranks form a straight are valid' do
		:given
		valid_straight_ranks = [
			[Ranks::ACE,   Ranks::TWO,   Ranks::THREE, Ranks::FOUR,  Ranks::FIVE], 
			[Ranks::TWO,   Ranks::THREE, Ranks::FOUR,  Ranks::FIVE,  Ranks::SIX],
			[Ranks::THREE, Ranks::FOUR,  Ranks::FIVE,  Ranks::SIX,   Ranks::SEVEN],
			[Ranks::FOUR,  Ranks::FIVE,  Ranks::SIX,   Ranks::SEVEN, Ranks::EIGHT],
			[Ranks::FIVE,  Ranks::SIX,   Ranks::SEVEN, Ranks::EIGHT, Ranks::NINE],
			[Ranks::SIX,   Ranks::SEVEN, Ranks::EIGHT, Ranks::NINE,  Ranks::TEN],
			[Ranks::SEVEN, Ranks::EIGHT, Ranks::NINE,  Ranks::TEN,   Ranks::JACK],
			[Ranks::EIGHT, Ranks::NINE,  Ranks::TEN,   Ranks::JACK,  Ranks::QUEEN],
			[Ranks::NINE,  Ranks::TEN,   Ranks::JACK,  Ranks::QUEEN, Ranks::KING],
			[Ranks::TEN,   Ranks::JACK,  Ranks::QUEEN, Ranks::KING,  Ranks::ACE] 
		]
		invalid_straight_ranks = [Ranks::JACK, Ranks::QUEEN, Ranks::KING, Ranks::ACE, Ranks::TWO]
		
		:expect
		valid_straight_ranks.all? { |ranks| HandRanks.new(*ranks).ranks_form_a_straight?.should == true }
		HandRanks.new(invalid_straight_ranks).ranks_form_a_straight?.should == false
	end

	it 'returns if ranks goes from ten to ace' do
		:given
		ranks_going_from_ten_to_ace = HandRanks.new Ranks::ACE, Ranks::TEN, Ranks::QUEEN, Ranks::JACK, Ranks::KING
		arbitrary_ranks = HandRanks.new Ranks::FOUR, Ranks::EIGHT, Ranks::TWO, Ranks::SEVEN, Ranks::KING

		:expect
		ranks_going_from_ten_to_ace.ranks_goes_from_ten_to_ace?.should == true
		arbitrary_ranks.ranks_goes_from_ten_to_ace?.should == false
	end

	it 'returns ranks grouped' do
		:given
		ranks = [Ranks::KING, Ranks::ACE, Ranks::ACE, Ranks::ACE, Ranks::ACE]

		:expect
		HandRanks.new(*ranks).ranks_grouped_are_equal_to?([1, 4]).should == true
	end
end